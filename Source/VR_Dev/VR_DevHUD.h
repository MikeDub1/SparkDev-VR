// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "VR_DevHUD.generated.h"

UCLASS()
class AVR_DevHUD : public AHUD
{
	GENERATED_BODY()

public:
	AVR_DevHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

